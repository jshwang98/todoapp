import { Checkbox, List, Typography, Space, Button } from 'antd'
import React, { useState } from 'react'
import { findIndex, get } from 'lodash'
import todoList from '.'
const { Item } = List
const { Text } = Typography
import AddForm from './components/addForm'
import TodoItem from './components/todoItem'

const TodoList = () => {
	const [todoData, setTodoData] = useState([
		{id:1, detail: 'Hwang yunseong', done:true},
		{id:2, detail: 'Cho Seungyoun', done:false}
	])	
  return (
    <div>
			<AddForm 
				todoData={todoData}
				setTodoData={setTodoData}
			/>
			<List
				bordered
				dataSource= {todoData}
				renderItem= {item => (
					<TodoItem
						todo={item}
						todoData={todoData}
						setTodoData={setTodoData}
					/>
				)}
			/>
    </div>
  )
}

export default TodoList