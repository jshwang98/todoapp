import React from 'react';
import {Form,Input,Button} from 'antd';
import { findIndex } from 'lodash';
import PropTypes, { array, func, object, shape } from 'prop-types';

const UpdateForm = ({ todoData, todo, setTodoData, toggleShowUpdate }) => {
  const onUpdateTodo = (values) => {
    console.log('update:', values)
    const { todoDetail } = values;
    const changedItemIndex = findIndex(todoData, {id: todo.id});
		const changedTodoData = [...todoData]
		changedTodoData[changedItemIndex]['detail'] = todoDetail
		console.log(changedTodoData);
		setTodoData(changedTodoData);
    toggleShowUpdate(false);
  }
  return(
    <Form
      initialValues={{
        todoDetail: todo.detail
      }}
      onFinish={onUpdateTodo}
    >
      <Form.Item
        name="todoDetail"
        style={{
          marginBottom: 0,
        }}
      >
        <Input value={todo.detail}/>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">update</Button>
      </Form.Item>
      </Form>
    )
}

UpdateForm.propTypes = {
  todoData: array.isRequired,
  todo: object.isRequired,
  setTodoData: func.isRequired,
  toggleShowUpdate: func.isRequired,
};
export default UpdateForm