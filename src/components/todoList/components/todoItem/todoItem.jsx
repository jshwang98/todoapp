import { Checkbox, List, Typography, Space, Button } from 'antd';
import React, { useState } from 'react';
import UpdateForm from '../updateForm';
import PropTypes, { array, func, object, shape } from 'prop-types';
import { filter, findIndex } from 'lodash';
const { Text } = Typography
const { Item } = List;

const TodoItem = ({ todoData, todo, setTodoData }) => {
  const [showUpdate, toggleShowUpdate] = useState(false)

  const onShowUpdate = () => {
    toggleShowUpdate(!showUpdate)
  }
  const onDeleteItem = () => {
    console.log("item removed",todo);
    const newTodoData = filter(todoData, (item)=>(item.id !== todo.id));
    console.log(newTodoData);
    setTodoData(newTodoData);
  }
  return (
    <Item>
      <Space direction="vertical">
        <div>
          <Space>
          <Checkbox
            checked={todo.done}
            onChange={(e) => {
              const changedItemIndex = findIndex(todoData, { id: todo.id });
              const changedTodoData = [...todoData]
              changedTodoData[changedItemIndex]['done'] = e.target.checked
              console.log(changedTodoData);
              setTodoData(changedTodoData);
            }}
          />
          {' '}
          <Text delete={todo.done}>{todo.detail}</Text>
          <Button disabled={todo.done} onClick={onShowUpdate} size="small">update</Button>
          <Button onClick={onDeleteItem} size="small">delete</Button>
          </Space>
        </div>
        {showUpdate && <div>
          <UpdateForm
            todoData={todoData}
            todo={todo}
            setTodoData={setTodoData}
            toggleShowUpdate={toggleShowUpdate}
          />
        </div>}
      </Space>
    </Item>
  )
}

TodoItem.propTypes = {
  todoData: array.isRequired,
  todo: object.isRequired,
  setTodoData: func.isRequired,
};

export default TodoItem;
