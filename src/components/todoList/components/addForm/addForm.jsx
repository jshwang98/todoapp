import React from 'react';
import { Form, Input, Button } from 'antd';
import { isEmpty, maxBy } from 'lodash';
import { array, func } from 'prop-types';

const AddForm = ({todoData, setTodoData}) => {
  const [form] = Form.useForm();
  const onAddTodo = (values) => {
    const { todoDetail } = values;
    if(!isEmpty(todoDetail)) {
      const newTodoData = [...todoData];
      const maxIndexItem = isEmpty(todoData) ? 0 : maxBy(todoData, (obj)=>(obj.id)).id;
      const newTodoItem = {
        id: maxIndexItem+1,
        detail: todoDetail,
        done: false
      }
      newTodoData.push(newTodoItem);
      console.log("new data",newTodoItem);
      console.log("new data list",newTodoData);
      setTodoData(newTodoData);
      form.resetFields();
    }
  }

  return(
    <Form
      form={form}
      onFinish={onAddTodo}
      layout="inline"
      initialValues={{
        todoDetail:""
      }}
    >
      <Form.Item
        name="todoDetail"
        rules={[
          {
            required: true,
            message: 'Todo detail must be valid when addTodo!',
          },
        ]}
        style={{
          marginBottom: 0,
        }}
      >
        <Input/>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">add</Button>
      </Form.Item>
      </Form>
  )
};

AddForm.propTypes = {
  todoData: array.isRequired,
  setTodoData: func.isRequired,
};

export default AddForm;
