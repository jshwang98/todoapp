import { Layout } from 'antd';
import TodoList from './components/todoList';
import 'antd/dist/antd.css';
import './App.css';
import React from 'react';

const { Header, Content } = Layout

function App() {
  return (
      <Layout>
      <Header>
        <h1 style={{color:"white"}}>Todo List</h1>
      </Header>
      <Content>
        <Layout>
          <TodoList />
        </Layout>
      </Content>
    </Layout>
  );
}

export default App;
